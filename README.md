![](https://gitlab.com/Antoniii/plot-van-pyscript/-/raw/main/index.png)

## Sources

* [Getting started with PyScript](https://github.com/pyscript/pyscript/blob/main/docs/tutorials/getting-started.md)
* [PyScript First Impressions - Run Python in HTML Document - RIP JavaScript](https://youtu.be/yrn1OYu9q0E)
